import com.loc.Client;
import com.loc.Film;
import com.loc.Location;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestSituation {

    private Client client;

    @Before
    public void setUp(){
        client = new Client("un client");
    }

    @After
    public void tearDown(){
        client = null;
    }

    @Test
    public void testFilmNormalMoinsDe3Jours() {
        Film film = new Film("Taxi Driver", Film.NORMAL);
        Location loc = new Location(film, 2);
        client.addLocation(loc);
        String attendu  = """
                Situation du client: un client
                \tTaxi Driver\t2.0
                Total du 2.0
                Vous gagnez 1 points de fidelite
                """;
        assertEquals(attendu, client.situation());
    }

    @Test
    public void testFilmNormalPlusDe3Jours() {
        Film film = new Film("Taxi Driver", Film.NORMAL);
        Location loc = new Location(film, 3);
        client.addLocation(loc);
        String attendu  = """
                Situation du client: un client
                \tTaxi Driver\t3.5
                Total du 3.5
                Vous gagnez 1 points de fidelite
                """;
        assertEquals(attendu, client.situation());
    }

    @Test
    public void testFilmNouveauteMoinsDe2Jours() {
        Film film = new Film("11 heures 14", Film.NOUVEAUTE);
        Location loc = new Location(film, 1);
        client.addLocation(loc);
        String attendu  = """
                Situation du client: un client
                \t11 heures 14\t3.0
                Total du 3.0
                Vous gagnez 1 points de fidelite
                """;
        assertEquals(attendu, client.situation());
    }

    @Test
    public void testFilmNouveautePlusDe2Jours() {
        Film film = new Film("11 heures 14", Film.NOUVEAUTE);
        Location loc = new Location(film, 4);
        client.addLocation(loc);
        String attendu  = """
                Situation du client: un client
                \t11 heures 14\t12.0
                Total du 12.0
                Vous gagnez 2 points de fidelite
                """;
        assertEquals(attendu, client.situation());
    }

    @Test
    public void testFilmEnfantMoinsDe4Jours() {
        Film film = new Film("Cendrillon", Film.ENFANT);
        Location loc = new Location(film, 3);
        client.addLocation(loc);
        String attendu  = """
                Situation du client: un client
                \tCendrillon\t1.5
                Total du 1.5
                Vous gagnez 1 points de fidelite
                """;
        assertEquals(attendu, client.situation());
    }

    @Test
    public void testFilmEnfantPlusDe4Jours() {
        Film film = new Film("Cendrillon", Film.ENFANT);
        Location loc = new Location(film, 4);
        client.addLocation(loc);
        String attendu  = """
                Situation du client: un client
                \tCendrillon\t3.0
                Total du 3.0
                Vous gagnez 1 points de fidelite
                """;
        assertEquals(attendu, client.situation());
    }

    @Test
    public void testCoffret3Jours() {
        Film film = new Film("Coffret Mentalist", Film.COFFRET_SERIES_TV);
        Location loc = new Location(film, 3);
        client.addLocation(loc);
        String attendu  = """
                Situation du client: un client
                \tCoffret Mentalist\t1.5
                Total du 1.5
                Vous gagnez 0 points de fidelite
                """;
        assertEquals(attendu, client.situation());
    }

    @Test
    public void testCinephile1Jour() {
        Film film = new Film("Il etait une fois dans l'ouest", Film.CINEPHILE);
        Location loc = new Location(film, 1);
        client.addLocation(loc);
        String attendu  = """
                Situation du client: un client
                \tIl etait une fois dans l'ouest\t2.0
                Total du 2.0
                Vous gagnez 3 points de fidelite
                """;
        assertEquals(attendu, client.situation());
    }

    @Test
    public void testCinephilePlusD1Jour() {
        Film film = new Film("Il etait une fois dans l'ouest", Film.CINEPHILE);
        Location loc = new Location(film, 4);
        client.addLocation(loc);
        String attendu  = """
                Situation du client: un client
                \tIl etait une fois dans l'ouest\t14.0
                Total du 14.0
                Vous gagnez 3 points de fidelite
                """;
        assertEquals(attendu, client.situation());
    }

    @Test
    public void testCumul() {
        Film filmEnfant = new Film("Cendrillon", Film.ENFANT);
        Location locEnfant = new Location(filmEnfant, 2);
        Film filmNouveaute = new Film("11 heures 14", Film.NOUVEAUTE);
        Location locNouveaute = new Location(filmNouveaute, 1);
        Film filmNormal = new Film("Taxi Driver", Film.NORMAL);
        Location locNormal = new Location(filmNormal, 2);
        Film filmCoffret = new Film("Coffret Mentalist", Film.COFFRET_SERIES_TV);
        Location locCoffret = new Location(filmCoffret, 1);
        Film filmCinephile = new Film("Il etait une fois dans l'ouest", Film.CINEPHILE);
        Location locCinephile = new Location(filmCinephile, 2);
        client.addLocation(locNormal);
        client.addLocation(locNouveaute);
        client.addLocation(locEnfant);
        client.addLocation(locCoffret);
        client.addLocation(locCinephile);
        String attendu = """
                Situation du client: un client
                \tTaxi Driver\t2.0
                \t11 heures 14\t3.0
                \tCendrillon\t1.5
                \tCoffret Mentalist\t0.5
                \tIl etait une fois dans l'ouest\t6.0
                Total du 13.0
                Vous gagnez 6 points de fidelite
                """;
        assertEquals(attendu, client.situation());
    }
}
