import com.loc.Client;
import com.loc.Film;
import com.loc.Location;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestSituationHTML {

    private Client client;

    @Before
    public void setUp(){
        client = new Client("un client");
    }

    @After
    public void tearDown(){
        client = null;
    }

    @Test
    public void testFilmNormalMoinsDe3Jours() {
        Film film = new Film("Taxi Driver", Film.NORMAL);
        Location loc = new Location(film, 2);
        client.addLocation(loc);
        String attendu  = """
                <!DOCTYPE html>
                <html lang="fr">
                <head>
                    <meta charset="UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <title>Situation du client</title>
                </head>
                <body>
                    <h2>Situation du client: un client</h2>
                    <ul>
                        <li>Taxi Driver - 2.0</li>
                    </ul>
                    <p>Total du 2.0</p>
                    <p>Vous gagnez 1 point de fidélité</p>
                </body>
                </html>
                """;
        assertEquals(attendu, client.situationEnHTML());
    }

    @Test
    public void testFilmNormalPlusDe3Jours() {
        Film film = new Film("Taxi Driver", Film.NORMAL);
        Location loc = new Location(film, 3);
        client.addLocation(loc);
        String attendu  = """
                <!DOCTYPE html>
                <html lang="fr">
                <head>
                    <meta charset="UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <title>Situation du client</title>
                </head>
                <body>
                    <h2>Situation du client: un client</h2>
                    <ul>
                        <li>Taxi Driver - 3.5</li>
                    </ul>
                    <p>Total du 3.5</p>
                    <p>Vous gagnez 1 point de fidélité</p>
                </body>
                </html>
                """;
        assertEquals(attendu, client.situationEnHTML());
    }

    @Test
    public void testFilmNouveauteMoinsDe2Jours() {
        Film film = new Film("11 heures 14", Film.NOUVEAUTE);
        Location loc = new Location(film, 1);
        client.addLocation(loc);
        String attendu  = """
                <!DOCTYPE html>
                <html lang="fr">
                <head>
                    <meta charset="UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <title>Situation du client</title>
                </head>
                <body>
                    <h2>Situation du client: un client</h2>
                    <ul>
                        <li>11 heures 14 - 3.0</li>
                    </ul>
                    <p>Total du 3.0</p>
                    <p>Vous gagnez 1 point de fidélité</p>
                </body>
                </html>
                """;
        assertEquals(attendu, client.situationEnHTML());
    }

    @Test
    public void testFilmNouveautePlusDe2Jours() {
        Film film = new Film("11 heures 14", Film.NOUVEAUTE);
        Location loc = new Location(film, 4);
        client.addLocation(loc);
        String attendu  = """
                <!DOCTYPE html>
                <html lang="fr">
                <head>
                    <meta charset="UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <title>Situation du client</title>
                </head>
                <body>
                    <h2>Situation du client: un client</h2>
                    <ul>
                        <li>11 heures 14 - 12.0</li>
                    </ul>
                    <p>Total du 12.0</p>
                    <p>Vous gagnez 2 point de fidélité</p>
                </body>
                </html>
                """;
        assertEquals(attendu, client.situationEnHTML());
    }

    @Test
    public void testFilmEnfantMoinsDe4Jours() {
        Film film = new Film("Cendrillon", Film.ENFANT);
        Location loc = new Location(film, 3);
        client.addLocation(loc);
        String attendu  = """
                <!DOCTYPE html>
                <html lang="fr">
                <head>
                    <meta charset="UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <title>Situation du client</title>
                </head>
                <body>
                    <h2>Situation du client: un client</h2>
                    <ul>
                        <li>Cendrillon - 1.5</li>
                    </ul>
                    <p>Total du 1.5</p>
                    <p>Vous gagnez 1 point de fidélité</p>
                </body>
                </html>
                """;
        assertEquals(attendu, client.situationEnHTML());
    }

    @Test
    public void testFilmEnfantPlusDe4Jours() {
        Film film = new Film("Cendrillon", Film.ENFANT);
        Location loc = new Location(film, 4);
        client.addLocation(loc);
        String attendu  = """
                <!DOCTYPE html>
                <html lang="fr">
                <head>
                    <meta charset="UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <title>Situation du client</title>
                </head>
                <body>
                    <h2>Situation du client: un client</h2>
                    <ul>
                        <li>Cendrillon - 3.0</li>
                    </ul>
                    <p>Total du 3.0</p>
                    <p>Vous gagnez 1 point de fidélité</p>
                </body>
                </html>
                """;
        assertEquals(attendu, client.situationEnHTML());
    }

    @Test
    public void testCoffret3Jours() {
        Film film = new Film("Coffret Mentalist", Film.COFFRET_SERIES_TV);
        Location loc = new Location(film, 3);
        client.addLocation(loc);
        String attendu  = """
                <!DOCTYPE html>
                <html lang="fr">
                <head>
                    <meta charset="UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <title>Situation du client</title>
                </head>
                <body>
                    <h2>Situation du client: un client</h2>
                    <ul>
                        <li>Coffret Mentalist - 1.5</li>
                    </ul>
                    <p>Total du 1.5</p>
                    <p>Vous gagnez 0 point de fidélité</p>
                </body>
                </html>
                """;
        assertEquals(attendu, client.situationEnHTML());
    }

    @Test
    public void testCinephile1Jour() {
        Film film = new Film("Il etait une fois dans l'ouest", Film.CINEPHILE);
        Location loc = new Location(film, 1);
        client.addLocation(loc);
        String attendu  = """
                <!DOCTYPE html>
                <html lang="fr">
                <head>
                    <meta charset="UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <title>Situation du client</title>
                </head>
                <body>
                    <h2>Situation du client: un client</h2>
                    <ul>
                        <li>Il etait une fois dans l'ouest - 2.0</li>
                    </ul>
                    <p>Total du 2.0</p>
                    <p>Vous gagnez 3 point de fidélité</p>
                </body>
                </html>
                """;
        assertEquals(attendu, client.situationEnHTML());
    }

    @Test
    public void testCinephilePlusD1Jour() {
        Film film = new Film("Il etait une fois dans l'ouest", Film.CINEPHILE);
        Location loc = new Location(film, 4);
        client.addLocation(loc);
        String attendu  = """
                <!DOCTYPE html>
                <html lang="fr">
                <head>
                    <meta charset="UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <title>Situation du client</title>
                </head>
                <body>
                    <h2>Situation du client: un client</h2>
                    <ul>
                        <li>Il etait une fois dans l'ouest - 14.0</li>
                    </ul>
                    <p>Total du 14.0</p>
                    <p>Vous gagnez 3 point de fidélité</p>
                </body>
                </html>
                """;
        assertEquals(attendu, client.situationEnHTML());
    }

    @Test
    public void testCumul() {
        Film filmEnfant = new Film("Cendrillon", Film.ENFANT);
        Location locEnfant = new Location(filmEnfant, 2);
        Film filmNouveaute = new Film("11 heures 14", Film.NOUVEAUTE);
        Location locNouveaute = new Location(filmNouveaute, 1);
        Film filmNormal = new Film("Taxi Driver", Film.NORMAL);
        Location locNormal = new Location(filmNormal, 2);
        Film filmCoffret = new Film("Coffret Mentalist", Film.COFFRET_SERIES_TV);
        Location locCoffret = new Location(filmCoffret, 1);
        Film filmCinephile = new Film("Il etait une fois dans l'ouest", Film.CINEPHILE);
        Location locCinephile = new Location(filmCinephile, 2);
        client.addLocation(locNormal);
        client.addLocation(locNouveaute);
        client.addLocation(locEnfant);
        client.addLocation(locCoffret);
        client.addLocation(locCinephile);
        String attendu = """
                <!DOCTYPE html>
                <html lang="fr">
                <head>
                    <meta charset="UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <title>Situation du client</title>
                </head>
                <body>
                    <h2>Situation du client: un client</h2>
                    <ul>
                        <li>Taxi Driver - 2.0</li>
                        <li>11 heures 14 - 3.0</li>
                        <li>Cendrillon - 1.5</li>
                        <li>Coffret Mentalist - 0.5</li>
                        <li>Il etait une fois dans l'ouest - 6.0</li>
                    </ul>
                    <p>Total du 13.0</p>
                    <p>Vous gagnez 6 point de fidélité</p>
                </body>
                </html>
                """;
        assertEquals(attendu, client.situationEnHTML());
    }
}
