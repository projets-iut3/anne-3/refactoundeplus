package com.loc.prix;

public class PrixNouveaute extends Prix {

    public PrixNouveaute() {
        super(1);
    }

    @Override
    public double getMontant(int nbJours) {
        return nbJours * 3;
    }

    @Override
    public int getPointsFidelite(int nbJours) {
        return nbJours > 1 ?
                2:
                1;
    }
}
