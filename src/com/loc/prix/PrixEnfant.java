package com.loc.prix;

public class PrixEnfant extends Prix {

    public PrixEnfant() {
        super(2);
    }

    @Override
    public double getMontant(int nbJours) {
        return nbJours > 3 ?
                (nbJours - 3) * 1.5 + 1.5 :
                1.5;
    }

    @Override
    public int getPointsFidelite(int nbJours) {
        return 1;
    }
}
