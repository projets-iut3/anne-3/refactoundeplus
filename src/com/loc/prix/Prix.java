package com.loc.prix;

public abstract class Prix {
    private int codePrix;

    public Prix(int codePrix) {
        this.codePrix = codePrix;
    }

    public int getCodePrix() {
        return codePrix;
    }

    public abstract double getMontant(int nbJours);
    public abstract int getPointsFidelite(int nbJours);
}
