package com.loc.prix;

public class PrixCinephile extends Prix {
    public PrixCinephile() {
        super(4);
    }

    @Override
    public double getMontant(int nbJours) {
        return nbJours > 1 ?
                2 + (nbJours - 1) * 4 :
                2;
    }

    @Override
    public int getPointsFidelite(int nbJours) {
        return 3;
    }


}
