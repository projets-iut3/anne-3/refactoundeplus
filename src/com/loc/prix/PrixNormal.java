package com.loc.prix;

public class PrixNormal extends Prix{
    public PrixNormal() {
        super(0);
    }

    @Override
    public double getMontant(int nbJours) {
        return nbJours > 2 ?
                2 + (nbJours - 2) * 1.5 :
                2;
    }

    @Override
    public int getPointsFidelite(int nbJours) {
        return 1;
    }
}
