package com.loc.prix;

public class PrixCoffret extends Prix {
    public PrixCoffret() {
        super(3);
    }

    @Override
    public double getMontant(int nbJours) {
        return nbJours * 0.5;
    }

    @Override
    public int getPointsFidelite(int nbJours) {
        return 0;
    }
}
