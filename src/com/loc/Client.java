package com.loc;

import com.loc.Statement.HtmlStatement;
import com.loc.Statement.TextStatement;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Client {
	private String nom;
	private List<Location> locations = new LinkedList<>();
	
	public Client(String nom) {
		this.nom = nom;
	}
	
	public void addLocation(Location location) {
		this.locations.add(location);
	}
	
	public String getNom() {
		return this.nom;
	}
	
	public String situation() {
		return (new TextStatement()).getReport(
				locations,
				nom,
				getMontantTotal(locations.iterator()),
				getPointsFideliteTotal(locations.iterator())
		);
	}

	public String situationEnHTML() {
		return (new HtmlStatement()).getReport(
				locations,
				nom,
				getMontantTotal(locations.iterator()),
				getPointsFideliteTotal(locations.iterator())
		);
	}

	private static int getPointsFideliteTotal(Iterator<Location> forEach) {
		int totalPointsFidelite = 0;
		while (forEach.hasNext()) {
			Location locationCoutante = forEach.next();
			totalPointsFidelite += locationCoutante.getPointsFidelite();
		}
		return totalPointsFidelite;
	}

	private static double getMontantTotal(Iterator<Location> forEach) {
		double totalDu = 0;
		while (forEach.hasNext()) {
			Location locationCoutante = forEach.next();
			totalDu += locationCoutante.getMontant();
		}
		return totalDu;
	}

}
