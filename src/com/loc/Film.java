package com.loc;

import com.loc.prix.*;

public class Film {

	public static final int CINEPHILE = 4;
	public static final int COFFRET_SERIES_TV = 3;
	public static final int ENFANT = 2;
	public static final int NOUVEAUTE = 1;
	public static final int NORMAL = 0;
	
	private String titre;
	private Prix codePrix;
	
	public Film(String titre, int codePrix) {
		this.titre = titre;
        setCodePrix(codePrix);
	}

	public String getTitre() {
		return this.titre;
	}

	public void setCodePrix(int codePrix) {
        switch (codePrix) {
			case NORMAL :
				this.codePrix =  new PrixNormal();
				break;
			case NOUVEAUTE:
				this.codePrix = new PrixNouveaute();
				break;
            case COFFRET_SERIES_TV:
 				this.codePrix = new PrixCoffret();
				break;
            case ENFANT:
				this.codePrix =  new PrixEnfant();
				break;
            case CINEPHILE:
				this.codePrix = new PrixCinephile();
				break;
            default:
				throw new IllegalStateException("Unexpected value: " + codePrix);
        };
	}

	public int getCodePrix() {
		return this.codePrix.getCodePrix();
	}

	public double getMontant(int nbJours) {
        return codePrix.getMontant(nbJours);
	}

	public int getPointsFidelite(int nbJours) {
        return codePrix.getPointsFidelite(nbJours);
	}
}