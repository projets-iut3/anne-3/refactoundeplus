package com.loc.Statement;


import com.loc.Location;

import java.util.Iterator;
import java.util.List;

public class TextStatement extends Statement {

    @Override
    public String head(String nom) {
        return "Situation du client: " + nom + "\n";
    }

    @Override
    public String getListFilms(List<Location> locations) {
        Iterator<Location> forEach = locations.iterator();
        StringBuilder result = new StringBuilder();
        while (forEach.hasNext()) {
            Location locationCoutante = forEach.next();
            result.append("\t" + locationCoutante.getFilm().getTitre() + "\t" + locationCoutante.getMontant() + "\n");
        }
        return result.toString();
    }

    @Override
    public String getTotaux(double montantTotal, int pointsDeFideliteTotaux) {
        return "Total du " + montantTotal + "\n" +
                "Vous gagnez " + pointsDeFideliteTotaux + " points de fidelite\n";
    }
}
