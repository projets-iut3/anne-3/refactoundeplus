package com.loc.Statement;

import com.loc.Location;

import java.util.Iterator;
import java.util.List;

public class HtmlStatement extends Statement {

    @Override
    public String head(String nom) {
        return "<!DOCTYPE html>\n" +
                "<html lang=\"fr\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" +
                "    <title>Situation du client</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "    <h2>Situation du client: " + nom + "</h2>\n" +
                "    <ul>\n";
    }

    @Override
    public String getListFilms(List<Location> locations) {
        Iterator<Location> forEach = locations.iterator();
        StringBuilder result = new StringBuilder();
        while (forEach.hasNext()) {
            Location locationCoutante = forEach.next();
            result.append("        <li>" + locationCoutante.getFilm().getTitre() + " - " +  locationCoutante.getMontant() + "</li>\n");
        }
        return result.toString();
    }

    @Override
    public String getTotaux(double montantTotal, int pointsDeFideliteTotaux) {
        return "    </ul>\n" +
                "    <p>Total du " + montantTotal + "</p>\n" +
                "    <p>Vous gagnez " + pointsDeFideliteTotaux + " point de fidélité</p>\n" +
                "</body>\n" +
                "</html>\n";
    }
}
