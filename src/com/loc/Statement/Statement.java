package com.loc.Statement;

import com.loc.Location;

import java.util.List;

public abstract class Statement {
    public abstract String head(String nom);
    public abstract String getListFilms(List<Location> locations);
    public abstract String getTotaux(double montantTotal, int pointsDeFideliteTotaux);

    public String getReport(List<Location> locations, String nom, double montantTotal, int pointsDeFideliteTotaux){
        return head(nom) + getListFilms(locations) + getTotaux(montantTotal, pointsDeFideliteTotaux);
    }
}
